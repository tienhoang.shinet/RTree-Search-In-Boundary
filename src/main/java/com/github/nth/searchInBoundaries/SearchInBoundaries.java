package com.github.nth.searchInBoundaries;

import com.github.nth.rtree.Entry;
import com.github.nth.rtree.RTree;
import com.github.nth.rtree.Utilities;
import com.github.nth.rtree.geometry.Point;
import rx.functions.Func1;

import java.util.*;

import static com.github.nth.rtree.geometry.Geometries.point;

/**
 * Created by Admin on 9/3/2015.
 */
public class SearchInBoundaries {

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        char ch;
        double lat;
        double lng;

        RTree<String, Point> tree = RTree.star().create();
        tree = initRTree(tree);

        do
        {
            steps();
            System.out.println("Enter double latitude: ");
            lat = scan.nextDouble();
            System.out.println("Enter double longitude: ");
            lng = scan.nextDouble();
            System.out.println("Display district: ");
            // search district
            long l = System.currentTimeMillis();
            List<Entry<String, Point>> list = tree.search(point(lat, lng), 0.01).toList().toBlocking().single();
            if (list.size() > 0) {
                Set<String> names = new TreeSet<String>();
                for (Entry entry : list) {
                    names.add(entry.value().toString());
                }
                for (final String name : names) {
                    List<Entry<String, Point>> entryList = tree.entries()
                            .filter(new Func1<Entry<String, Point>, Boolean>() {
                                @Override
                                public Boolean call(Entry<String, Point> entry) {
                                    return entry.value().equals(name);
                                }
                            }).toList()
                            .toBlocking().single();
                    List<Point> entryNewList = new ArrayList<Point>();
                    for (Entry<String, Point> entry : entryList) {
                        entryNewList.add(entry.geometry());
                    }

                    if (coordinateInRegion(entryNewList, point(lat, lng))) {
                        System.out.println(name);
                        break;
                    }
                }
            } else {
                System.out.print("No result");
            }
            System.out.print(System.currentTimeMillis() - l);
            // ask to continue
            System.out.println("\nDo you want to continue (Type y or n) \n");
            ch = scan.next().charAt(0);
        } while (ch == 'Y'|| ch == 'y');
    }

    static boolean coordinateInRegion(List<Point> boundaries, Point coord) {
        int i, j;
        boolean isInside = false;
        //create an array of coordinates from the region boundary list
        Point[] verts = boundaries.toArray(new Point[boundaries.size()]);
        int sides = verts.length;
        for (i = 0, j = sides - 1; i < sides; j = i++) {
            //verifying if your coordinate is inside your region
            if (
                    (
                            (verts[i].y() <= coord.y()) && (coord.y() <= verts[j].y())
                    ) || (
                            (verts[j].y() <= coord.y()) && (coord.y() <= verts[i].y())
                    ) &&
                            (coord.x() <= (verts[j].x() - verts[i].x()) * (coord.y() - verts[i].y()) / (verts[j].y() - verts[i].y()) + verts[i].x())
            ) {
                isInside = !isInside;
            }
        }
        return isInside;
    }

    private static RTree<String, Point> initRTree(RTree rTree) {
        rTree = Utilities.entries50000("/50000_1.txt", rTree);
        rTree = Utilities.entries50000("/50000_2.txt", rTree);
        return rTree;
    }

    private static void steps() {
        System.out.println("1. insert Latitude");
        System.out.println("2. insert Longitude");
        System.out.println("3. check location");
    }
}
