package com.github.nth.rtree;

import static java.util.Collections.min;

import java.util.List;

import com.github.nth.rtree.geometry.Geometry;

/**
 * Uses minimal area increase to select a node from a list.
 *
 */
public final class SelectorMinimalAreaIncrease implements Selector {

    @SuppressWarnings("unchecked")
    @Override
    public <T, S extends Geometry> Node<T, S> select(Geometry g, List<? extends Node<T, S>> nodes) {
        return min(nodes, Comparators.compose(Comparators.areaIncreaseComparator(g.mbr()), Comparators.areaComparator(g.mbr())));
    }
}
