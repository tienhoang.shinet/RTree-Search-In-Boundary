package com.github.nth.rtree;

import static com.github.nth.rtree.Comparators.areaComparator;
import static com.github.nth.rtree.Comparators.areaIncreaseComparator;
import static com.github.nth.rtree.Comparators.compose;
import static com.github.nth.rtree.Comparators.overlapAreaComparator;
import static java.util.Collections.min;

import java.util.List;

import com.github.nth.rtree.geometry.Geometry;

public final class SelectorMinimalOverlapArea implements Selector {

    @SuppressWarnings("unchecked")
    @Override
    public <T, S extends Geometry> Node<T, S> select(Geometry g, List<? extends Node<T, S>> nodes) {
        return min(
                nodes,
                compose(overlapAreaComparator(g.mbr(), nodes), areaIncreaseComparator(g.mbr()),
                        areaComparator(g.mbr())));
    }

}
