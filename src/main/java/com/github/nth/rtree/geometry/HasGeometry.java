package com.github.nth.rtree.geometry;

public interface HasGeometry {

    Geometry geometry();
}
