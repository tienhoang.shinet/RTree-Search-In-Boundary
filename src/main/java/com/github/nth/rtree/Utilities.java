package com.github.nth.rtree;

import com.github.nth.rtree.geometry.Geometries;
import com.github.nth.rtree.geometry.Point;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Utilities {

    public static RTree<String, Point> entries50000(String filePath, RTree<String, Point> tree) {
        BufferedReader br = new BufferedReader(new InputStreamReader(
                RTree.class.getResourceAsStream(filePath)));
        String line;

        try {
            String district = "";
            while ((line = br.readLine()) != null) {
                if (line.contains("name")) {
                    district = line.split("name:")[1];
                } else {
                    String[] items = line.split(",");
                    double lat = Double.parseDouble(items[1]);
                    double lng = Double.parseDouble(items[0]);
                    tree = tree.add(district, Geometries.point(lat, lng));
                }
            }
            br.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tree;
    }
}
